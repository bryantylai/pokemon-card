﻿using PokeCard.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Search;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace PokeCard
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public static Boolean firstLaunched = true;
        public static Boolean noInternet = false;
        public static StorageFolder roamingFolder = null;
        public const string filename = "FavoritePkmnFile.txt";
        private SearchPane searchPane;
        public static MainPage Current;
        public static ObservableCollection<Cards> FavCollection = new ObservableCollection<Cards>();
        public static ObservableCollection<Favs> CacheCollection = new ObservableCollection<Favs>();
        public ObservableCollection<Series> SeriesCollection = new ObservableCollection<Series>();

        String[] exSet = {"ruby-and-sapphire", "sandstorm", "dragon", "team-magma-vs-team-aqua", "hidden-legends",
                           "firered-and-leafgreen", "team-rocket-returns", "deoxys", "emerald", "unseen-forces", "delta-species",
                           "legend-maker", "holon-phantoms", "crystal-guardians", "dragon-frontiers", "power-keepers" };
        String[] dpSet = {"diamond-and-pearl","mysterious-treasures","secret-wonders","great-encounters","majestic-dawn",
                           "legends-awaken","stormfront" };
        String[] platSet = { "platinum", "rising-rivals", "supreme-victors", "arceus" };
        String[] colSet = { "heartgold-and-soulsilver", "unleashed", "undaunted", "triumphant", "call-of-legends" };
        String[] bwSet = { "black-and-white", "emerging-powers", "noble-victories", "next-destinies", "dark-explorers", "dragons-exalted",
                             "dragon-vault", "boundaries-crossed", "plasma-storm", "plasma-freeze"};
        String[] typeSet = { "grass", "fire", "water", "lightning", "psychic", "fighting", "darkness", "metal", "dragon", "colorless" };


        public MainPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;

            Current = this;
            searchPane = SearchPane.GetForCurrentView();
            searchPane.ShowOnKeyboardInput = true;

            roamingFolder = ApplicationData.Current.RoamingFolder;

            Series exSeries = new Series() { SeriesName = "Ex Series" };
            Series dpSeries = new Series() { SeriesName = "Diamond & Pearl Series" };
            Series platSeries = new Series() { SeriesName = "Platinum Series" };
            Series colSeries = new Series() { SeriesName = "Call of Legends & HGSS Series" };
            Series bwSeries = new Series() { SeriesName = "Black & White Series" };

            exSeries.SetCollection = new ObservableCollection<Sets>();
            dpSeries.SetCollection = new ObservableCollection<Sets>();
            platSeries.SetCollection = new ObservableCollection<Sets>();
            colSeries.SetCollection = new ObservableCollection<Sets>();
            bwSeries.SetCollection = new ObservableCollection<Sets>();

            foreach (String item in exSet)
            {
                Sets set = new Sets();
                set.SetName = "ex-" + item;
                set.ImageUri = new Uri("ms-appx:/Assets/Sets/ex-" + item + ".png", UriKind.RelativeOrAbsolute);
                exSeries.SetCollection.Insert(0, set);
            }

            foreach (String item in dpSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("ms-appx:/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                dpSeries.SetCollection.Insert(0, set);
            }

            foreach (String item in platSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("ms-appx:/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                platSeries.SetCollection.Insert(0, set);
            }

            foreach (String item in colSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("ms-appx:/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                colSeries.SetCollection.Insert(0, set);
            }

            foreach (String item in bwSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("ms-appx:/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                bwSeries.SetCollection.Insert(0, set);
            }

            SeriesCollection.Insert(0, exSeries);
            SeriesCollection.Insert(0, dpSeries);
            SeriesCollection.Insert(0, platSeries);
            SeriesCollection.Insert(0, colSeries);
            SeriesCollection.Insert(0, bwSeries);

            CollectionViewSource collections = new CollectionViewSource();
            collections.IsSourceGrouped = true;
            collections.Source = SeriesCollection;
            collections.ItemsPath = new PropertyPath("SetCollection");

            itemsGrid.ItemsSource = collections.View;

            itemsGrid.SelectedItem = null;
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            try
            {
                HttpResponseMessage response = await new HttpClient().GetAsync("http://www.facebook.com");
            }
            catch (Exception)
            {
                noInternet = true;
            }

            if (firstLaunched)
                LoadFavoriteFile();
        }

        private void itemsGrid_ItemClick(object sender, ItemClickEventArgs e)
        {
            Sets selected = (Sets)e.ClickedItem;
            this.Frame.Navigate(typeof(SetsPage), selected.SetName);
        }

        internal void ProcessSearchQuery(String terms)
        {
            while(this.Frame.CanGoBack)
                this.Frame.GoBack();
            this.Frame.Navigate(typeof(ResultsPage), terms);
        }

        public static async void RemoveFromFile()
        {
            StorageFile file = await roamingFolder.CreateFileAsync(filename, CreationCollisionOption.OpenIfExists);
            string saveText = "";

            foreach (Cards card in FavCollection)
            {
                saveText += card.ImageUri + "\t";
            }

            await FileIO.WriteTextAsync(file, saveText);

            await new MessageDialog("This Pokemon Card is removed from your favorites list.").ShowAsync();
        }

        public static async void SaveToFavoriteFile(Cards card)
        {
            String imageFilename = card.ImageUri.OriginalString;
            imageFilename = imageFilename.Substring(imageFilename.LastIndexOf("/") - 3).Replace("/", "-");

            StorageFile imageFile = await roamingFolder.CreateFileAsync(imageFilename, CreationCollisionOption.ReplaceExisting);

            var client = new HttpClient();
            var clientResponse = await client.GetByteArrayAsync(card.ImageUri);

            try
            {
                await FileIO.WriteBytesAsync(imageFile, clientResponse);
            }
            catch (Exception)
            {
                new MessageDialog("Sorry, an unexpected error occured while saving to favorites. Please contact developer when this happens. Thank you.").ShowAsync();
                return;
            }

            StorageFile file = await roamingFolder.CreateFileAsync(filename, CreationCollisionOption.OpenIfExists);
            string saveText = "";

            saveText += card.ImageUri + "\t";

            await FileIO.AppendTextAsync(file, saveText);

            await new MessageDialog("This Pokemon Card is added into your favorites list.").ShowAsync();

        }

        public async void LoadFavoriteFile()
        {
            string text = "";
            try
            {
                StorageFile file = await roamingFolder.GetFileAsync(filename);
                text = await FileIO.ReadTextAsync(file);

                String[] bits = text.Split('\t');
                for (int i = 0; i < bits.Length - 1; i++)
                {
                    FavCollection.Add(new Cards() { ImageUri = new Uri(bits[i], UriKind.RelativeOrAbsolute) });
                }

                if (noInternet)
                {
                    for (int i = 0; i < bits.Length - 1; i++)
                    {
                        Uri newUri = new Uri(bits[i], UriKind.RelativeOrAbsolute);
                        String imageFilename = newUri.OriginalString;
                        imageFilename = imageFilename.Substring(imageFilename.LastIndexOf("/") - 3).Replace("/", "-");

                        StorageFile imageFile = await roamingFolder.GetFileAsync(imageFilename);

                        var clientResponse = await imageFile.OpenStreamForReadAsync();

                        var randomAccessStream = new MemoryRandomAccessStream(clientResponse);
                        BitmapImage img = new BitmapImage();
                        await img.SetSourceAsync(randomAccessStream);
                        CacheCollection.Add(new Favs() { ImageUri = img, ActualUri = newUri });
                    }
                }
            }
            catch (Exception)
            {
            }

            firstLaunched = false;
        }

        private void favoriteButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(FavoritesPage));
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}
