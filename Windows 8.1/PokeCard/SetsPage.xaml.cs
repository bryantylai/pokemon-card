﻿using PokeCard.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace PokeCard
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class SetsPage : Page
    {

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public static ObservableCollection<Cards> CardCollection = new ObservableCollection<Cards>();

        public SetsPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;

            itemsGrid.ItemsSource = CardCollection;
        }

        private void itemsGrid_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Frame.Navigate(typeof(CardsDetailPage), (Cards)e.ClickedItem);
        }

        private void favoriteButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(FavoritesPage));
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            CardCollection.Clear();
            UpdateLayout();

            if (MainPage.noInternet)
            {
                await new MessageDialog("Pokemon Card Gallery is unable to load the Pokemon Cards. Please check your internet connection.").ShowAsync();
                return;
            }

            if (CardCollection.Count == 0)
            {
                string title = (String)e.NavigationParameter;
                int max = 0;
                string uri = "";

                switch (title)
                {
                    case "ex-ruby-and-sapphire":
                        max = 109;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX1/EX1_EN_";
                        break;
                    case "ex-sandstorm":
                        max = 100;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX2/EX2_EN_";
                        break;
                    case "ex-dragon":
                        max = 97;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX3/EX3_EN_";
                        break;
                    case "ex-team-magma-vs-team-aqua":
                        max = 95;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX4/EX4_EN_";
                        break;
                    case "ex-hidden-legends":
                        max = 101;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX5/EX5_EN_";
                        break;
                    case "ex-firered-and-leafgreen":
                        max = 112;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX6/EX6_EN_";
                        break;
                    case "ex-team-rocket-returns":
                        max = 109;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX7/EX7_EN_";
                        break;
                    case "ex-deoxys":
                        max = 106;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX8/EX8_EN__";
                        break;
                    case "ex-emerald":
                        max = 106;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX9/EX9_EN_";
                        break;
                    case "ex-unseen-forces":
                        max = 144;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX10/EX10_EN_";
                        break;
                    case "ex-delta-species":
                        max = 114;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX11/EX11_EN_";
                        break;
                    case "ex-legend-maker":
                        max = 93;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX12/EX12_EN_";
                        break;
                    case "ex-holon-phantoms":
                        max = 111;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX13/EX13_EN_";
                        break;
                    case "ex-crystal-guardians":
                        max = 100;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX14/EX14_EN_";
                        break;
                    case "ex-dragon-frontiers":
                        max = 101;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX15/EX15_EN_";
                        break;
                    case "ex-power-keepers":
                        max = 108;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX16/EX16_EN_";
                        break;
                    case "diamond-and-pearl":
                        max = 130;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP1/DP1_EN_";
                        break;
                    case "mysterious-treasures":
                        max = 124;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP2/DP2_EN_";
                        break;
                    case "secret-wonders":
                        max = 132;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP3/DP3_EN_";
                        break;
                    case "great-encounters":
                        max = 106;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP4/DP4_EN_";
                        break;
                    case "majestic-dawn":
                        max = 100;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP5/DP5_EN_";
                        break;
                    case "legends-awaken":
                        max = 146;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP6/DP6_EN_";
                        break;
                    case "stormfront":
                        max = 106;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP7/DP7_EN_";
                        break;
                    case "platinum":
                        max = 133;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/PL1/PL1_EN_";
                        break;
                    case "rising-rivals":
                        max = 120;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/PL2/PL2_EN_";
                        break;
                    case "supreme-victors":
                        max = 153;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/PL3/PL3_EN_";
                        break;
                    case "arceus":
                        max = 111;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/PL4/PL4_EN_";
                        break;
                    case "heartgold-and-soulsilver":
                        max = 116;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/HGSS1/HGSS1_EN_";
                        break;
                    case "unleashed":
                        max = 96;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/HGSS2/HGSS2_EN_";
                        break;
                    case "undaunted":
                        max = 91;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/HGSS3/HGSS3_EN_";
                        break;
                    case "triumphant":
                        max = 103;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/HGSS4/HGSS4_EN_";
                        break;
                    case "call-of-legends":
                        max = 98;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/COL1/COL1_EN_";
                        break;
                    case "black-and-white":
                        max = 107;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW1/BW1_EN_";
                        break;
                    case "emerging-powers":
                        max = 98;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW2/BW2_EN_";
                        break;
                    case "noble-victories":
                        max = 102;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW3/BW3_EN_";
                        break;
                    case "next-destinies":
                        max = 103;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW4/BW4_EN_";
                        break;
                    case "dark-explorers":
                        max = 111;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW5/BW5_EN_";
                        break;
                    case "dragons-exalted":
                        max = 128;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW6/BW6_EN_";
                        break;
                    case "dragon-vault":
                        max = 20;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DV1/DV1_EN_";
                        break;
                    case "boundaries-crossed":
                        max = 153;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW7/BW7_EN_";
                        break;
                    case "plasma-storm":
                        max = 135;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW8/BW8_EN_";
                        break;
                    case "plasma-freeze":
                        max = 116;
                        uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW9/BW9_EN_";
                        break;
                }

                for (int index = 1; index <= max; index++)
                {
                    CardCollection.Add(new Cards() { ImageUri = new Uri(uri + index + ".jpg", UriKind.RelativeOrAbsolute) });
                }
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}
