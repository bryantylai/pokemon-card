﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokeWP
{
    public class Sets
    {
        public Uri ImageUri { get; set; }
        public String SetName { get; set; }
    }

    public class Cards
    {
        public Uri ImageUri { get; set; }
    }
}
