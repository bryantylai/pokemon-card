﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.Windows.Threading;

namespace PokeWP
{
    public partial class MainPage : PhoneApplicationPage
    {
        String[] exSet = {"ruby-and-sapphire", "sandstorm", "dragon", "team-magma-vs-team-aqua", "hidden-legends",
                           "firered-and-leafgreen", "team-rocket-returns", "deoxys", "emerald", "unseen-forces", "delta-species",
                           "legend-maker", "holon-phantoms", "crystal-guardians", "dragon-frontiers", "power-keepers" };
        String[] dpSet = {"diamond-and-pearl","mysterious-treasures","secret-wonders","great-encounters","majestic-dawn",
                           "legends-awaken","stormfront" };
        String[] platSet = { "platinum", "rising-rivals", "supreme-victors", "arceus" };
        String[] colSet = { "heartgold-and-soulsilver", "unleashed", "undaunted", "triumphant", "call-of-legends" };
        String[] bwSet = { "black-and-white", "emerging-powers", "noble-victories", "next-destinies", "dark-explorers", "dragons-exalted",
                             "dragon-vault", "boundaries-crossed", "plasma-storm", "plasma-freeze"};

        DispatcherTimer timer;

        public MainPage()
        {
            InitializeComponent();

            bwPanoramaItem.Header = "black & white";
            colPanoramaItem.Header = "call of legends";
            platPanoramaItem.Header = "platinum";
            dpPanoramaItem.Header = "diamond pearl";
            exPanoramaItem.Header = "ex";

            ObservableCollection<Sets> exCollection = new ObservableCollection<Sets>();
            ObservableCollection<Sets> dpCollection = new ObservableCollection<Sets>();
            ObservableCollection<Sets> platCollection = new ObservableCollection<Sets>();
            ObservableCollection<Sets> colCollection = new ObservableCollection<Sets>();
            ObservableCollection<Sets> bwCollection = new ObservableCollection<Sets>();

            foreach (String item in exSet)
            {
                Sets set = new Sets();
                set.SetName = "ex-" + item;
                set.ImageUri = new Uri("/Assets/Sets/ex-" + item + ".png", UriKind.RelativeOrAbsolute);
                exCollection.Insert(0, set);
            }

            foreach (String item in dpSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                dpCollection.Insert(0, set);
            }

            foreach (String item in platSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                platCollection.Insert(0, set);
            }

            foreach (String item in colSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                colCollection.Insert(0, set);
            }

            foreach (String item in bwSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                bwCollection.Insert(0, set);
            }

            bwList.ItemsSource = bwCollection;
            colList.ItemsSource = colCollection;
            platList.ItemsSource = platCollection;
            dpList.ItemsSource = dpCollection;
            exList.ItemsSource = exCollection;
        }

        private void bwList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void colList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void platList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dpList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void exList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}