﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeCardWP
{
    public class Sets
    {
        public Uri ImageUri { get; set; }
        public String SetName { get; set; }
    }

    public class Cards
    {
        public Uri ImageUri { get; set; }
    }
}
