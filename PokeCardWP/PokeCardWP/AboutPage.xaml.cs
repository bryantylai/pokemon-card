﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Xml.Linq;

namespace PokeCardWP
{
    public partial class AboutPage : PhoneApplicationPage
    {
        public AboutPage()
        {
            InitializeComponent();

            versionText.Text = "Version " + XDocument.Load("WMAppManifest.xml").Root.Element("App").Attribute("Version").Value;

            emailButton.Label = "Email";
            emailButton.ImageUri = new Uri("Assets/Email.png", UriKind.RelativeOrAbsolute);

            reviewButton.Label = "Rate & Review";
            reviewButton.ImageUri = new Uri("Assets/Review.png", UriKind.RelativeOrAbsolute);

            UpdateLayout();
        }

        private void emailButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            EmailComposeTask task = new EmailComposeTask();
            task.Subject = "Regarding Pokemon Card Gallery";
            task.To = "bryanlai@mystudentpartners.com";
            task.Show();
        }

        private void reviewButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MarketplaceReviewTask task = new MarketplaceReviewTask();
            task.Show();
        }
    }
}