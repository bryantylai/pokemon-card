﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;
using Microsoft.Xna.Framework.Media;
using System.IO.IsolatedStorage;
using System.IO;
using System.Windows.Resources;

namespace PokeCardWP
{
    public partial class CardPage : PhoneApplicationPage
    {
        String uri = "";
        public CardPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            uri = NavigationContext.QueryString["uri"];
            img.Source = new BitmapImage(new Uri(uri, UriKind.RelativeOrAbsolute));
        }

        private void saveAppBarButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (MemoryStream tempMemoryStream = new MemoryStream())
                {
                    WriteableBitmap wb = new WriteableBitmap((int)img.ActualWidth, (int)img.ActualHeight);
                    wb.Render(img, null);
                    wb.Invalidate();
                    wb.SaveJpeg(tempMemoryStream, wb.PixelWidth, wb.PixelHeight, 0, 100);
                    tempMemoryStream.SetLength(tempMemoryStream.Position);
                    tempMemoryStream.Seek(0, SeekOrigin.Begin);
                    MediaLibrary mediaLibrary = new MediaLibrary();
                    mediaLibrary.SavePicture("Untitled.jpg", tempMemoryStream);

                    MessageBox.Show("This Pokemon Card has been saved into your Photos Hub.");
                }
            }
            catch (Exception ex)
            {
                if (MessageBox.Show("Unfortunately, an error has occured while saving the Pokemon Card. Would you like to report this issue?", "", MessageBoxButton.OKCancel).Equals(MessageBoxResult.OK))
                {
                    EmailComposeTask task = new EmailComposeTask();
                    task.Subject = "Error Occured While Saving Pokemon Card Image";
                    task.Body = "\n\n" + ex.Message; 
                    task.To = "bryanlai@mystudentpartners.com";
                    task.Show();
                }
            }
        }

        private void shareAppBarButton_Click(object sender, EventArgs e)
        {
            ShareLinkTask task = new ShareLinkTask();
            task.LinkUri = new Uri(uri, UriKind.RelativeOrAbsolute);
            task.Message = "Check out this cool Pokemon Card #PokemonCardGallery #Pokemon #PokemonCard";
            task.Show();
        }

        private void rateAppBarMenuItem_Click(object sender, EventArgs e)
        {
            MarketplaceReviewTask task = new MarketplaceReviewTask();
            task.Show();
        }

        private void aboutAppBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void searhAppBarButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/SearchPage.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}