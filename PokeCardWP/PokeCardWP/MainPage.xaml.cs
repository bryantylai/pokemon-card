﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.Net.NetworkInformation;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace PokeCardWP
{
    public partial class MainPage : PhoneApplicationPage
    {
        String[] exSet = {"ruby-and-sapphire", "sandstorm", "dragon", "team-magma-vs-team-aqua", "hidden-legends",
                           "firered-and-leafgreen", "team-rocket-returns", "deoxys", "emerald", "unseen-forces", "delta-species",
                           "legend-maker", "holon-phantoms", "crystal-guardians", "dragon-frontiers", "power-keepers" };
        String[] dpSet = {"diamond-and-pearl","mysterious-treasures","secret-wonders","great-encounters","majestic-dawn",
                           "legends-awaken","stormfront" };
        String[] platSet = { "platinum", "rising-rivals", "supreme-victors", "arceus" };
        String[] colSet = { "heartgold-and-soulsilver", "unleashed", "undaunted", "triumphant", "call-of-legends" };
        String[] bwSet = { "black-and-white", "emerging-powers", "noble-victories", "next-destinies", "dark-explorers", "dragons-exalted",
                             "dragon-vault", "boundaries-crossed", "plasma-storm", "plasma-freeze"};

        public MainPage()
        {
            InitializeComponent();

            bwText.Text = "black & white";
            colText.Text = "call of legends";
            platText.Text = "platinum";
            dpText.Text = "diamond pearl";
            exText.Text = "ex";

            ObservableCollection<Sets> exCollection = new ObservableCollection<Sets>();
            ObservableCollection<Sets> dpCollection = new ObservableCollection<Sets>();
            ObservableCollection<Sets> platCollection = new ObservableCollection<Sets>();
            ObservableCollection<Sets> colCollection = new ObservableCollection<Sets>();
            ObservableCollection<Sets> bwCollection = new ObservableCollection<Sets>();

            foreach (String item in exSet)
            {
                Sets set = new Sets();
                set.SetName = "ex-" + item;
                set.ImageUri = new Uri("/Assets/Sets/ex-" + item + ".png", UriKind.RelativeOrAbsolute);
                exCollection.Insert(0, set);
            }

            foreach (String item in dpSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                dpCollection.Insert(0, set);
            }

            foreach (String item in platSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                platCollection.Insert(0, set);
            }

            foreach (String item in colSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                colCollection.Insert(0, set);
            }

            foreach (String item in bwSet)
            {
                Sets set = new Sets();
                set.SetName = item;
                set.ImageUri = new Uri("/Assets/Sets/" + item + ".png", UriKind.RelativeOrAbsolute);
                bwCollection.Insert(0, set);
            }

            bwList.ItemsSource = bwCollection;
            colList.ItemsSource = colCollection;
            platList.ItemsSource = platCollection;
            dpList.ItemsSource = dpCollection;
            exList.ItemsSource = exCollection;

            GenerateAppBar();

            if (!NetworkInterface.GetIsNetworkAvailable())
                MessageBox.Show("Please ensure you are connected to internet to view Pokemon Cards.");
        }

        private void GenerateAppBar()
        {
            ApplicationBar = new ApplicationBar();
            ApplicationBar.BackgroundColor = Color.FromArgb(255, 208, 29, 36);

            ApplicationBarIconButton searchAppBarButton = new ApplicationBarIconButton();
            searchAppBarButton.Text = "search";
            searchAppBarButton.IconUri = new Uri("/Assets/AppBar/search.png", UriKind.RelativeOrAbsolute);
            searchAppBarButton.Click += searchAppBarButton_Click;

            ApplicationBarMenuItem reviewMenuItem = new ApplicationBarMenuItem();
            reviewMenuItem.Text = "rate and review";
            reviewMenuItem.Click += reviewMenuItem_Click;

            ApplicationBarMenuItem aboutMenuItem = new ApplicationBarMenuItem();
            aboutMenuItem.Text = "about";
            aboutMenuItem.Click += aboutMenuItem_Click;

            ApplicationBar.Buttons.Add(searchAppBarButton);
            ApplicationBar.MenuItems.Add(reviewMenuItem);
            ApplicationBar.MenuItems.Add(aboutMenuItem);
        }

        void aboutMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.RelativeOrAbsolute));
        }

        void reviewMenuItem_Click(object sender, EventArgs e)
        {
            MarketplaceReviewTask task = new MarketplaceReviewTask();
            task.Show();
        }

        void searchAppBarButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/SearchPage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void bwList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SetsPage.xaml?title=" + ((Sets)bwList.SelectedItem).SetName, UriKind.RelativeOrAbsolute));
        }

        private void colList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SetsPage.xaml?title=" + ((Sets)colList.SelectedItem).SetName, UriKind.RelativeOrAbsolute));
        }

        private void platList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SetsPage.xaml?title=" + ((Sets)platList.SelectedItem).SetName, UriKind.RelativeOrAbsolute));
        }

        private void dpList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SetsPage.xaml?title=" + ((Sets)dpList.SelectedItem).SetName, UriKind.RelativeOrAbsolute));
        }

        private void exList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SetsPage.xaml?title=" + ((Sets)exList.SelectedItem).SetName, UriKind.RelativeOrAbsolute));
        }
    }
}