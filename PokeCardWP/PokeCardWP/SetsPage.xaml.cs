﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using Microsoft.Phone.Tasks;
using System.Net.NetworkInformation;

namespace PokeCardWP
{
    public partial class SetsPage : PhoneApplicationPage
    {
        public SetsPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                MessageBox.Show("Please ensure you are connected to internet to view Pokemon Cards.");
                return;
            }

            String title = NavigationContext.QueryString["title"];
            Dispatcher.BeginInvoke(() => GetResults(title));

            SetTitle.Text = title.Replace("-", " ");
        }

        /// <summary>
        /// Get results from web
        /// </summary>
        /// <param name="uri">website uri</param>
        private void GetResults(String title)
        {
            int max = 0;
            string uri = "";

            switch (title)
            {
                case "ex-ruby-and-sapphire":
                    max = 109;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX1/EX1_EN_";
                    break;
                case "ex-sandstorm":
                    max = 100;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX2/EX2_EN_";
                    break;
                case "ex-dragon":
                    max = 97;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX3/EX3_EN_";
                    break;
                case "ex-team-magma-vs-team-aqua":
                    max = 95;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX4/EX4_EN_";
                    break;
                case "ex-hidden-legends":
                    max = 101;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX5/EX5_EN_";
                    break;
                case "ex-firered-and-leafgreen":
                    max = 112;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX6/EX6_EN_";
                    break;
                case "ex-team-rocket-returns":
                    max = 109;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX7/EX7_EN_";
                    break;
                case "ex-deoxys":
                    max = 106;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX8/EX8_EN__";
                    break;
                case "ex-emerald":
                    max = 106;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX9/EX9_EN_";
                    break;
                case "ex-unseen-forces":
                    max = 144;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX10/EX10_EN_";
                    break;
                case "ex-delta-species":
                    max = 114;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX11/EX11_EN_";
                    break;
                case "ex-legend-maker":
                    max = 93;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX12/EX12_EN_";
                    break;
                case "ex-holon-phantoms":
                    max = 111;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX13/EX13_EN_";
                    break;
                case "ex-crystal-guardians":
                    max = 100;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX14/EX14_EN_";
                    break;
                case "ex-dragon-frontiers":
                    max = 101;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX15/EX15_EN_";
                    break;
                case "ex-power-keepers":
                    max = 108;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/EX16/EX16_EN_";
                    break;
                case "diamond-and-pearl":
                    max = 130;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP1/DP1_EN_";
                    break;
                case "mysterious-treasures":
                    max = 124;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP2/DP2_EN_";
                    break;
                case "secret-wonders":
                    max = 132;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP3/DP3_EN_";
                    break;
                case "great-encounters":
                    max = 106;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP4/DP4_EN_";
                    break;
                case "majestic-dawn":
                    max = 100;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP5/DP5_EN_";
                    break;
                case "legends-awaken":
                    max = 146;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP6/DP6_EN_";
                    break;
                case "stormfront":
                    max = 106;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DP7/DP7_EN_";
                    break;
                case "platinum":
                    max = 133;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/PL1/PL1_EN_";
                    break;
                case "rising-rivals":
                    max = 120;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/PL2/PL2_EN_";
                    break;
                case "supreme-victors":
                    max = 153;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/PL3/PL3_EN_";
                    break;
                case "arceus":
                    max = 111;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/PL4/PL4_EN_";
                    break;
                case "heartgold-and-soulsilver":
                    max = 116;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/HGSS1/HGSS1_EN_";
                    break;
                case "unleashed":
                    max = 96;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/HGSS2/HGSS2_EN_";
                    break;
                case "undaunted":
                    max = 91;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/HGSS3/HGSS3_EN_";
                    break;
                case "triumphant":
                    max = 103;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/HGSS4/HGSS4_EN_";
                    break;
                case "call-of-legends":
                    max = 98;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/COL1/COL1_EN_";
                    break;
                case "black-and-white":
                    max = 107;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW1/BW1_EN_";
                    break;
                case "emerging-powers":
                    max = 98;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW2/BW2_EN_";
                    break;
                case "noble-victories":
                    max = 102;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW3/BW3_EN_";
                    break;
                case "next-destinies":
                    max = 103;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW4/BW4_EN_";
                    break;
                case "dark-explorers":
                    max = 111;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW5/BW5_EN_";
                    break;
                case "dragons-exalted":
                    max = 128;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW6/BW6_EN_";
                    break;
                case "dragon-vault":
                    max = 20;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/DV1/DV1_EN_";
                    break;
                case "boundaries-crossed":
                    max = 153;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW7/BW7_EN_";
                    break;
                case "plasma-storm":
                    max = 135;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW8/BW8_EN_";
                    break;
                case "plasma-freeze":
                    max = 116;
                    uri = "http://assets1.pokemon.com/assets/cms/img/cards/card_images_web_large/BW9/BW9_EN_";
                    break;

            } 
            
            ObservableCollection<Cards> CardCollection = new ObservableCollection<Cards>();
            
            for (int index = 1; index <= max; index++)
            {
                CardCollection.Add(new Cards() { ImageUri = new Uri(uri + index + ".jpg", UriKind.RelativeOrAbsolute) });
            }

            miniList.ItemsSource = CardCollection;
            largeList.ItemsSource = CardCollection;
        }

        private void switchAppBarButton_Click(object sender, EventArgs e)
        {
            if (largeList.Visibility.Equals(Visibility.Collapsed))
            {
                miniList.Margin = new Thickness(1000, 0, 0, 0);
                miniList.Visibility = Visibility.Collapsed;

                largeList.Margin = new Thickness(0, 0, 0, 0);
                largeList.Visibility = Visibility.Visible;
            }
            else
            {
                largeList.Margin = new Thickness(1000, 0, 0, 0);
                largeList.Visibility = Visibility.Collapsed;

                miniList.Margin = new Thickness(0, 0, 0, 0);
                miniList.Visibility = Visibility.Visible;
            }
        }

        private void largeList_Hold(object sender, System.Windows.Input.GestureEventArgs e)
        {
            largeList.Margin = new Thickness(1000, 0, 0, 0);
            largeList.Visibility = Visibility.Collapsed;

            miniList.Margin = new Thickness(0, 0, 0, 0);
            miniList.Visibility = Visibility.Visible;
        }

        private void miniList_Hold(object sender, System.Windows.Input.GestureEventArgs e)
        {
            miniList.Margin = new Thickness(1000, 0, 0, 0);
            miniList.Visibility = Visibility.Collapsed;

            largeList.Margin = new Thickness(0, 0, 0, 0);
            largeList.Visibility = Visibility.Visible;
        }

        private void miniList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/CardPage.xaml?uri=" + ((Cards)miniList.SelectedItem).ImageUri, UriKind.RelativeOrAbsolute));
        }

        private void largeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/CardPage.xaml?uri=" + ((Cards)largeList.SelectedItem).ImageUri, UriKind.RelativeOrAbsolute));
        }

        private void rateAppBarMenuItem_Click(object sender, EventArgs e)
        {
            MarketplaceReviewTask task = new MarketplaceReviewTask();
            task.Show();
        }

        private void aboutAppBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void searhAppBarButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/SearchPage.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}