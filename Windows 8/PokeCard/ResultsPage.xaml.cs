﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace PokeCard
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class ResultsPage : PokeCard.Common.LayoutAwarePage
    {
        public static ObservableCollection<Cards> CardCollection = new ObservableCollection<Cards>();

        public ResultsPage()
        {
            this.InitializeComponent();

            ApplicationViewStates.CurrentStateChanged += ApplicationViewStates_CurrentStateChanged;

            itemsGrid.ItemsSource = CardCollection;
            itemsList.ItemsSource = CardCollection;
        }

        void ApplicationViewStates_CurrentStateChanged(object sender, VisualStateChangedEventArgs e)
        {
            if (ApplicationView.Value.Equals(ApplicationViewState.Snapped))
            {
                itemsList.Visibility = Visibility.Visible;
                itemsGrid.Visibility = Visibility.Collapsed;
            }
            else
            {
                itemsList.Visibility = Visibility.Collapsed;
                itemsGrid.Visibility = Visibility.Visible;

            }
        }

        protected override void GoBack(object sender, RoutedEventArgs e)
        {
            base.GoBack(sender, e);

            CardCollection.Clear();
            UpdateLayout();

            if (this.Frame != null)
            {
                while (this.Frame.CanGoBack)
                    this.Frame.GoBack();
            }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected async override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            if (CardCollection.Count == 0 || (String)navigationParameter != "")
            {
                CardCollection.Clear();
                UpdateLayout();

                String toSearch = ((String)navigationParameter).ToLower();
                String resultPage = "";

                if (MainPage.noInternet)
                {
                    await new MessageDialog("Pokemon Card Gallery is unable to load the Pokemon Cards. Please check your internet connection.").ShowAsync();
                    return;
                }

                HttpClient httpClient = new HttpClient();
                Uri resource;

                switch (toSearch)
                {
                    case "fire":
                    case "grass":
                    case "water":
                    case "lightning":
                    case "psychic":
                    case "fighting":
                    case "darkness":
                    case "metal":
                    case "dragon":
                    case "colorless":
                    case "colourless":
                        if (toSearch.Equals("colourless"))
                            toSearch = "colorless";
                        resultPage = "http://www.pokemon.com/us/pokemon-trading-card-game/database/basic/?cardSearchTerm=&searchCardName=&card-" + toSearch + "=&sort=Position&searchDatabase=Submit";
                        
                        if (!Uri.TryCreate(resultPage, UriKind.RelativeOrAbsolute, out resource))
                        {
                            await new MessageDialog("Uri " + resource.ToString() + " Invalid").ShowAsync();
                        }

                        try
                        {
                            HttpResponseMessage response = await httpClient.GetAsync(resource);
                            String result = await response.Content.ReadAsStringAsync();

                            MatchCollection cofunCheck = Regex.Matches(result, "<h3 class=\"cufon right\" id=\"positionIndicator\">.*?</h3>", RegexOptions.Singleline);
                            if (cofunCheck.Count == 0)
                            {
                                await new MessageDialog("There isn't any Pokemon Card which matches your search terms.").ShowAsync();
                                return;
                            }
                            else
                            {
                                string get = Regex.Replace(cofunCheck[0].Value.ToString(), @"<[^>]*>", String.Empty);
                                string[] bits = get.Split(' ');
                                int loops = Convert.ToInt32(bits[2]);
                                int turns = 1;
                                while (loops > 10)
                                {
                                    loops -= 10;
                                    turns++;
                                }

                                for (int i = 0; i < turns; i++)
                                {
                                    string uriResults = "";
                                    if (i != 0)
                                    {
                                        uriResults = "http://www.pokemon.com/us/pokemon-trading-card-game/database/basic/" + (i + 1) + "?cardSearchTerm=&searchCardName=&card-" + toSearch + "=&sort=Position&searchDatabase=Submit";

                                        if (!Uri.TryCreate(uriResults, UriKind.RelativeOrAbsolute, out resource))
                                        {
                                            await new MessageDialog("Uri " + resource.ToString() + " Invalid").ShowAsync();
                                        }

                                        response = await httpClient.GetAsync(resource);
                                        result = await response.Content.ReadAsStringAsync();
                                    }

                                    MatchCollection mc1 = Regex.Matches(result, "<div class=\"cdbResult\">.*?</div>", RegexOptions.Singleline);

                                    for (int j = 0; j < mc1.Count; j++)
                                    {
                                        MatchCollection mc2 = Regex.Matches(mc1[j].ToString(), "<img class=\"cardImage\".*?/>", RegexOptions.Singleline);
                                        MatchCollection mc3 = Regex.Matches(mc2[0].Value, "src=\".*?\"");
                                        string link = mc3[0].Value.Replace("src=", "").Replace("\"", "");
                                        CardCollection.Add(new Cards() { ImageUri = new Uri(link.Replace("thumbnails_1", "large"), UriKind.RelativeOrAbsolute) });

                                    }
                                }
                            }

                            return;
                        }
                        catch (HttpRequestException)
                        {

                        }
                        break;
                    default:
                        resultPage = "http://www.pokemon.com/us/pokemon-trading-card-game/database/basic/?cardSearchTerm=" + toSearch + "&searchCardName=&sort=Position&searchDatabase=Submit";
                        
                        if (!Uri.TryCreate(resultPage, UriKind.RelativeOrAbsolute, out resource))
                        {
                            await new MessageDialog("Uri " + resource.ToString() + " Invalid").ShowAsync();
                        }

                        try
                        {
                            HttpResponseMessage response = await httpClient.GetAsync(resource);
                            String result = await response.Content.ReadAsStringAsync();

                            MatchCollection cofunCheck = Regex.Matches(result, "<h3 class=\"cufon right\" id=\"positionIndicator\">.*?</h3>", RegexOptions.Singleline);
                            if (cofunCheck.Count == 0)
                            {
                                await new MessageDialog("There isn't any Pokemon Card which matches your search terms.").ShowAsync();
                                return;
                            }
                            else
                            {
                                string get = Regex.Replace(cofunCheck[0].Value.ToString(), @"<[^>]*>", String.Empty);
                                string[] bits = get.Split(' ');
                                int loops = Convert.ToInt32(bits[2]);
                                int turns = 1;
                                while (loops > 10)
                                {
                                    loops -= 10;
                                    turns++;
                                }

                                for (int i = 0; i < turns; i++)
                                {
                                    string uriResults = "";
                                    if (i != 0)
                                    {
                                        uriResults = "http://www.pokemon.com/us/pokemon-trading-card-game/database/basic/" + (i + 1) + "/?cardSearchTerm=" + toSearch + "&searchCardName=&sort=Position&searchDatabase=Submit";

                                        if (!Uri.TryCreate(uriResults, UriKind.RelativeOrAbsolute, out resource))
                                        {
                                            await new MessageDialog("Uri " + resource.ToString() + " Invalid").ShowAsync();
                                        }

                                        response = await httpClient.GetAsync(resource);
                                        result = await response.Content.ReadAsStringAsync();
                                    }

                                    MatchCollection mc1 = Regex.Matches(result, "<div class=\"cdbResult\">.*?</div>", RegexOptions.Singleline);

                                    for (int j = 0; j < mc1.Count; j++)
                                    {
                                        MatchCollection mc2 = Regex.Matches(mc1[j].ToString(), "<img class=\"cardImage\".*?/>", RegexOptions.Singleline);
                                        MatchCollection mc3 = Regex.Matches(mc2[0].Value, "src=\".*?\"");
                                        string link = mc3[0].Value.Replace("src=", "").Replace("\"", "");
                                        CardCollection.Add(new Cards() { ImageUri = new Uri(link.Replace("thumbnails_1", "large"), UriKind.RelativeOrAbsolute) });

                                    }
                                }
                            }

                            return;
                        }
                        catch (HttpRequestException)
                        {

                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void itemsGrid_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Frame.Navigate(typeof(CardResultsPage), (Cards)e.ClickedItem);
        }

        private void itemsList_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Frame.Navigate(typeof(CardResultsPage), (Cards)e.ClickedItem);
        }

        private void favoriteButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(FavoritesPage));
        }
    }
}
