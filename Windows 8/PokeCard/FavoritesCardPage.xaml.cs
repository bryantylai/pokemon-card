﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace PokeCard
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class FavoritesCardPage : PokeCard.Common.LayoutAwarePage
    {
        public FavoritesCardPage()
        {
            this.InitializeComponent();

            ApplicationViewStates.CurrentStateChanged += ApplicationViewStates_CurrentStateChanged;

            if (MainPage.noInternet)
            {
                itemsFlip.ItemsSource = MainPage.CacheCollection;
                snappedFlip.ItemsSource = MainPage.CacheCollection;
            }
            else
            {
                itemsFlip.ItemsSource = MainPage.FavCollection;
                snappedFlip.ItemsSource = MainPage.FavCollection;
            }
        }

        void ApplicationViewStates_CurrentStateChanged(object sender, VisualStateChangedEventArgs e)
        {
            if (ApplicationView.Value.Equals(ApplicationViewState.Snapped))
            {
                snappedFlip.Visibility = Visibility.Visible;
                itemsFlip.Visibility = Visibility.Collapsed;

                snappedFlip.SelectedItem = itemsFlip.SelectedItem;
            }
            else
            {
                snappedFlip.Visibility = Visibility.Collapsed;
                itemsFlip.Visibility = Visibility.Visible;

                itemsFlip.SelectedItem = snappedFlip.SelectedItem;
            }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            if (MainPage.noInternet)
            {
                Favs selected = (Favs)navigationParameter;
                itemsFlip.SelectedItem = selected;
                snappedFlip.SelectedItem = selected;
            }
            else
            {
                Cards selected = (Cards)navigationParameter;
                itemsFlip.SelectedItem = selected;
                snappedFlip.SelectedItem = selected;
            }

            //itemsFlip.SelectedItem = selected;
            //snappedFlip.SelectedItem = selected;

            // Register for DataRequested events
            DataTransferManager.GetForCurrentView().DataRequested += OnDataRequested;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
            // Deregister the DataRequested event handler
            DataTransferManager.GetForCurrentView().DataRequested -= OnDataRequested;
        }

        void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            var request = args.Request;

            request.Data.Properties.Title = "Pokemon Card Gallery";

            if (!MainPage.noInternet)
            {
                request.Data.SetUri(((Cards)itemsFlip.SelectedItem).ImageUri);

                RandomAccessStreamReference reference = RandomAccessStreamReference.CreateFromUri(((Cards)itemsFlip.SelectedItem).ImageUri);
                request.Data.Properties.Thumbnail = reference;

                request.Data.SetBitmap(reference);
            }
        }

        private void unfavoriteButton_Click(object sender, RoutedEventArgs e)
        {
            if (ApplicationView.Value.Equals(ApplicationViewState.Snapped))
            {
                if (!MainPage.noInternet)
                    MainPage.FavCollection.Remove((Cards)snappedFlip.SelectedItem);
                else
                {
                    foreach (Cards item in MainPage.FavCollection)
                    {
                        if (item.ImageUri.Equals(((Favs)snappedFlip.SelectedItem).ActualUri))
                        {
                            MainPage.FavCollection.Remove(item);
                            break;
                        }
                    }
                    MainPage.CacheCollection.Remove((Favs)snappedFlip.SelectedItem);
                }
            }
            else
            {
                if (!MainPage.noInternet)
                    MainPage.FavCollection.Remove((Cards)itemsFlip.SelectedItem);
                else
                {
                    foreach (Cards item in MainPage.FavCollection)
                    {
                        if (item.ImageUri.Equals(((Favs)itemsFlip.SelectedItem).ActualUri))
                        {
                            MainPage.FavCollection.Remove(item);
                            break;
                        }
                    }
                    MainPage.CacheCollection.Remove((Favs)itemsFlip.SelectedItem);
                }
            }

            MainPage.RemoveFromFile();

            if (MainPage.FavCollection.Count == 0)
                this.Frame.GoBack();
        }
    }
}