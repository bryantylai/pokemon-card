﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace PokeCard
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class FavoritesPage : PokeCard.Common.LayoutAwarePage
    {
        public FavoritesPage()
        {
            this.InitializeComponent();

            ApplicationViewStates.CurrentStateChanged += ApplicationViewStates_CurrentStateChanged;

            if (MainPage.noInternet)
            {
                itemsGrid.ItemsSource = MainPage.CacheCollection;
                itemsList.ItemsSource = MainPage.CacheCollection;
            }
            else
            {
                itemsGrid.ItemsSource = MainPage.FavCollection;
                itemsList.ItemsSource = MainPage.FavCollection;
            }
        }

        void ApplicationViewStates_CurrentStateChanged(object sender, VisualStateChangedEventArgs e)
        {
            if (ApplicationView.Value.Equals(ApplicationViewState.Snapped))
            {
                itemsList.Visibility = Visibility.Visible;
                itemsGrid.Visibility = Visibility.Collapsed;
            }
            else
            {
                itemsList.Visibility = Visibility.Collapsed;
                itemsGrid.Visibility = Visibility.Visible;

            }
        }

        protected override void GoBack(object sender, RoutedEventArgs e)
        {
            base.GoBack(sender, e);
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void itemsGrid_ItemClick(object sender, ItemClickEventArgs e)
        {
            if(MainPage.noInternet)
                this.Frame.Navigate(typeof(FavoritesCardPage), (Favs)e.ClickedItem);
            else
                this.Frame.Navigate(typeof(FavoritesCardPage), (Cards)e.ClickedItem);
        }

        private void itemsList_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (MainPage.noInternet)
                this.Frame.Navigate(typeof(FavoritesCardPage), (Favs)e.ClickedItem);
            else
                this.Frame.Navigate(typeof(FavoritesCardPage), (Cards)e.ClickedItem);
        }
    }
}
