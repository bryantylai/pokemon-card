﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PokeCard
{
    public sealed partial class AboutUserControl : UserControl
    {
        public AboutUserControl()
        {
            this.InitializeComponent();
            about.Text = "Developed By \nBryan Lai \n\nMicrosoft Student Partners Malaysia";
        }

        private async void HyperlinkButton_Click_1(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("mailto:bryanlai@mystudentpartners.com", UriKind.Absolute);
            await Launcher.LaunchUriAsync(uri);
        }
    }
}
