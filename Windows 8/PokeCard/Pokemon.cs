﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace PokeCard
{
    public class Sets
    {
        public Uri ImageUri { get; set; }
        public String SetName { get; set; }
    }

    public class Series
    {
        public ObservableCollection<Sets> SetCollection { get; set; }
        public String SeriesName { get; set; }
    }

    public class Cards
    {
        public Uri ImageUri { get; set; }
    }

    public class Favs
    {
        public BitmapImage ImageUri { get; set; }
        public Uri ActualUri { get; set; }
    }
}
